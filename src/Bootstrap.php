<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   15-Sep-17
 */

namespace alexs\yii2sitemap;
use yii\base\BootstrapInterface;
use yii\web\Application;

class Bootstrap implements BootstrapInterface
{
    public function bootstrap($app) {
        $app->on(Application::EVENT_BEFORE_REQUEST, function() use ($app) {
            $app->getUrlManager()->addRules([
                ['pattern'=>'sitemap', 'route'=>'sitemap/generate', 'suffix'=>'.xml']
            ]);
        });
    }
}
