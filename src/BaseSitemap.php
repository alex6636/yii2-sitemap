<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   15-Sep-17
 */

namespace alexs\yii2sitemap;
use Yii;
use DomainException;

abstract class BaseSitemap
{
    public $cache_duration = 86400; // 1 day
    public $cache_key = 'sitemap';
    public $additional_urls = [];

    /** 
     * @throws DomainException
     */
    public function __construct() {
        if (Yii::$app->cache === NULL) {
            throw new DomainException('The cache component is not configured');
        }
    }
    
    /** 
     * @return array 
     */
    public abstract function getUrls();

    /**
     * @param string $url
     */
    public function setAdditionalUrl($url) {
        $this->additional_urls[] = $url;
    }

    /**
     * @return string
     */
    public function generate() {
        if ($cache = Yii::$app->cache->get($this->cache_key)) {
            return $cache;
        }
        if (!empty($this->additional_urls)) {
            $urls = array_keys(array_merge(array_flip($this->getUrls()), array_flip($this->additional_urls)));
        } else {
            $urls = $this->getUrls();
        }
        $contents  = '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
        foreach ($urls as $url) {
            $contents .= '<url><loc>' . $url . '</loc></url>';
        }
        $contents .= '</urlset>';
        if ($this->cache_duration) {
            Yii::$app->cache->set($this->cache_key, $contents, $this->cache_duration);  
        }
        return $contents;
    }
}