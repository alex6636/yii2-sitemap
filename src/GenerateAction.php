<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   15-Sep-17
 */

namespace alexs\yii2sitemap;
use yii\base\Action;
use Yii;
use yii\web\Response;

class GenerateAction extends Action
{
    /** @var BaseSitemap $SitemapClass */
    public $SitemapClass;

    /**
     * @return string
     */
    public function run() {
        if (!$this->SitemapClass instanceof BaseSitemap) {
            throw new \DomainException('The sitemap class is invalid');
        }
        Yii::$app->response->format = Response::FORMAT_RAW;
        Yii::$app->response->headers->add('Content-Type', 'application/xml');
        return $this->SitemapClass->generate();
    }
}