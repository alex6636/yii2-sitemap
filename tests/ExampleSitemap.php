<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   14-Sep-17
 */

namespace alexs\yii2sitemap\tests;
use alexs\yii2sitemap\BaseSitemap;

class ExampleSitemap extends BaseSitemap
{
    public function getUrls() {
        $domain = 'http://examplesite.com';
        $urls = [
            $domain,
            $domain . '/article',
            $domain . '/another-article',
        ];
        return $urls;
    }
}