<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   11-Sep-17
 */

namespace alexs\yii2sitemap\tests;
use alexs\yii2phpunittestcase\TestCase;
use Yii;
use yii\web\Application;

class GenerateActionTest extends TestCase
{
    public function testGeneratedContentType() {
        Yii::$app->runAction('/sitemap/generate');
        $this->assertEquals('application/xml', Yii::$app->response->headers->get('Content-Type'));
    }

    /** @expectedException \DomainException */
    public function testInvalidConfigException() {
        Yii::$app->runAction('/sitemap-invalid/generate');
    }

    protected function setUp() {
        parent::setUp();
        Yii::$app->controllerNamespace = 'alexs\\yii2sitemap\\tests\\controllers';
    }

    protected function mockApplication() {
        new Application([
            'id'=>'test_app',
            'basePath'=>__DIR__,
            'vendorPath'=>dirname(__DIR__) . '/vendor',
            'components'=>[
                'cache'=>[
                    'class'=>'yii\caching\FileCache',
                ],
            ],
        ]);
    }
}
