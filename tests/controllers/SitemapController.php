<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   15-Sep-17
 */

namespace alexs\yii2sitemap\tests\controllers;
use yii\web\Controller;
use alexs\yii2sitemap\tests\ExampleSitemap;

class SitemapController extends Controller
{
    public function actions() {
        return [
            'generate'=>[
                'class'=>'alexs\yii2sitemap\GenerateAction',
                'SitemapClass'=>new ExampleSitemap,
            ],
        ];
    }
}