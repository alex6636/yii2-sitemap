<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   15-Sep-17
 */

namespace alexs\yii2sitemap\tests\controllers;
use yii\web\Controller;

class SitemapInvalidController extends Controller
{
    public function actions() {
        return [
            'generate'=>[
                'class'=>'alexs\yii2sitemap\GenerateAction',
            ],
        ];
    }
}