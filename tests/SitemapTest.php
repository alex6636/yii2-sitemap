<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   11-Sep-17
 */

namespace alexs\yii2sitemap\tests;
use alexs\yii2phpunittestcase\TestCase;
use Yii;
use yii\web\Application;

class SitemapTest extends TestCase
{
    public function testGenerate() {
        $ExampleSitemap = new ExampleSitemap;
        $xml = $ExampleSitemap->generate();
        $expected =  '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' .
                     '<url><loc>http://examplesite.com</loc></url>' .
                     '<url><loc>http://examplesite.com/article</loc></url>' .
                     '<url><loc>http://examplesite.com/another-article</loc></url>' .
                     '</urlset>';
        $this->assertEquals($expected, $xml);
    }

    public function testAdditionalUrl() {
        $ExampleSitemap = new ExampleSitemap;
        $ExampleSitemap->setAdditionalUrl('http://examplesite.com/additional');
        $xml = $ExampleSitemap->generate();
        $expected =  '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' .
                     '<url><loc>http://examplesite.com</loc></url>' .
                     '<url><loc>http://examplesite.com/article</loc></url>' .
                     '<url><loc>http://examplesite.com/another-article</loc></url>' .
                     '<url><loc>http://examplesite.com/additional</loc></url>' .
                     '</urlset>';
        $this->assertEquals($expected, $xml);
    }

    public function testCache() {
        $ExampleSitemap = new ExampleSitemap;
        $ExampleSitemap->cache_duration = 2;
        $xml = $ExampleSitemap->generate();
        $expected =  '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' .
                     '<url><loc>http://examplesite.com</loc></url>' .
                     '<url><loc>http://examplesite.com/article</loc></url>' .
                     '<url><loc>http://examplesite.com/another-article</loc></url>' .
                     '</urlset>';
        $this->assertEquals($expected, $xml);

        $ExampleSitemap->setAdditionalUrl('http://examplesite.com/additional');
        $xml = $ExampleSitemap->generate();
        $this->assertEquals($expected, $xml);
        sleep(3);
        $xml = $ExampleSitemap->generate();
        $expected =  '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' .
                     '<url><loc>http://examplesite.com</loc></url>' .
                     '<url><loc>http://examplesite.com/article</loc></url>' .
                     '<url><loc>http://examplesite.com/another-article</loc></url>' .
                     '<url><loc>http://examplesite.com/additional</loc></url>' .
                     '</urlset>';
        $this->assertEquals($expected, $xml);
    }

    protected function mockApplication() {
        new Application([
            'id'=>'test_app',
            'basePath'=>__DIR__,
            'vendorPath'=>dirname(__DIR__) . '/vendor',
            'components'=>[
                'cache'=>[
                    'class'=>'yii\caching\FileCache',
                ],
            ],
        ]);
        Yii::$app->cache->delete('sitemap');
    }
}
