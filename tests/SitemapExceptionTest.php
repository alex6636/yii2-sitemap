<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   11-Sep-17
 */

namespace alexs\yii2sitemap\tests;
use alexs\yii2phpunittestcase\TestCase;
use yii\web\Application;

class SitemapExceptionTest extends TestCase
{
    /** @expectedException \DomainException */
    public function testException() {
        new ExampleSitemap;
    }

    protected function mockApplication() {
        new Application([
            'id'=>'test_app',
            'basePath'=>__DIR__,
            'vendorPath'=>dirname(__DIR__) . '/vendor',
            'components'=>[],
        ]);
    }
}
