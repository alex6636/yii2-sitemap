# README #

Sitemap extension for Yii2

### Rules ###
You'll need to create your sitemap, for example:

```php
<?php
namespace app\components;
use alexs\yii2sitemap\BaseSitemap;
use yii\helpers\Url;

class Sitemap extends BaseSitemap
{
    public function getUrls() {
        $domain = 'http://examplesite.com';
        $static_urls = [
            $domain,
            $domain . '/article',
            $domain . '/another-article',
        ];
        $dynamic_urls = [];
        $dynamic_urls[] = Url::to(['/url/to'], true);
        return array_merge($static_urls, $dynamic_urls);
    }
}
```

And controller:
```php
<?php
namespace app\controllers;
use app\components\Sitemap;
use yii\web\controller;

class SitemapController extends Controller
{
    public function actions() {
        return [
            'generate'=>[
                'class'=>'alexs\yii2sitemap\GenerateAction',
                'SitemapClass'=>new Sitemap,
            ],
        ];
    }
}
```

You can follow the url <your_site_base_url>/sitemap.xml
The option **enablePrettyUrl** should have **true** value!